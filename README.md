## Problem Statement for the task
Design a program to implement the specifications mentioned in the problem statement below.   
Make sure your solution is validated against the test cases included in the problem statement   
and can satisfy similar test cases. You are free to write additional test cases to validate your solution,   
if required. The program can be written in the programming language of your choice. We would prefer your   
code to be checked in on any source code management system (Github, Bitbucket, etc).      
The solution must include the source code and/or build scripts, if required.    
The solution must contain a runnable client to test the solution end to end.

## Technology
Java Spring Boot + Maven

## IDE
IntelliJ IDEA 2017.2.6

## To access client use URL: http://localhost:8080/upload

## Test:
   LawnTests.java
   MowerTests.java
   