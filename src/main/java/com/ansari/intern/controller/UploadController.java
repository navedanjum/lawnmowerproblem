package com.ansari.intern.controller;

import com.ansari.intern.domain.Lawn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Controller
public class UploadController {

    @PostMapping(value = "/upload")
    public String upload(@RequestParam("file") MultipartFile multipartFile, Model model) throws IOException {
        String content = new String(multipartFile.getBytes());
        log.info(content);
        Lawn lawn;
        try {
            lawn = Lawn.inputFileParser(content);
        }catch (Exception ex) {
            model.addAttribute("errorMessage","Bad input:"+ ex.getMessage());
            return "upload-file";
        }
        model.addAttribute("outputString", lawn.startMower());
        return "upload-file";
    }

    @GetMapping("/upload")
    public String renderPage() {
        return "upload-file";
    }

    @ExceptionHandler(RuntimeException.class)
    void handleIllegalArgumentException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

}
