package com.ansari.intern.domain;

public enum CommandType {
    FORWARD,
    LEFT,
    RIGHT
}
