package com.ansari.intern.domain;

public enum Orientation {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
