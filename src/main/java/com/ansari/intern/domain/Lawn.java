package com.ansari.intern.domain;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.ansari.intern.helper.FileParser.validateInputFile;
import static com.ansari.intern.helper.MapBuilder.ORIENTATION_NAME;
import static com.ansari.intern.helper.MapBuilder.getKeyFromValue;
import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Integer.parseInt;
import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;
import static org.thymeleaf.util.StringUtils.split;

@Data
@Slf4j
public class Lawn {
    private final int length;
    private final int width;
    private final List<Mower> mowers;


    public static Lawn inputFileParser(String content) {
        log.info("Parsing Input command file");
        String[] lines = validateInputFile(content);
        String[] dimension = split(lines[0], " ");
        int length = parseInt(dimension[0]);
        int width = parseInt(dimension[1]);
        int numberOfMowers = (lines.length - 1) / 2;
        List<Mower> mowers = newArrayList();
        for (int i = 0; i < numberOfMowers; i++) {
            mowers.add(Mower.inputCommandParser(lines[(2 * i) + 1], lines[(2 * i) + 2], length, width));

        }

        return new Lawn(length, width, mowers);
    }


    public String startMower() {
        StringBuffer finalPositions = new StringBuffer("");
        for (Mower mower : mowers) {
            mower.executeCommand();
            log.info("Result output");
            log.info(mower.getXcoord() + " " + mower.getYcoord() + " " +
                    getKeyFromValue(ORIENTATION_NAME, mower.getOrientation()));

            finalPositions.append(mower.getXcoord() + " " + mower.getYcoord() + " " +
                    getKeyFromValue(ORIENTATION_NAME, mower.getOrientation()) + "<br/>");
        }

        return finalPositions.toString();
    }
}
