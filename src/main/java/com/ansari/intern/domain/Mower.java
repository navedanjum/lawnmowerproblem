package com.ansari.intern.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.ansari.intern.helper.FileParser.validateCommandSequence;
import static com.ansari.intern.helper.FileParser.validateMowerPosition;
import static com.ansari.intern.helper.MapBuilder.*;
import static java.lang.Integer.parseInt;


@Data
@AllArgsConstructor
@Slf4j
public class Mower {
    private int xcoord;
    private int ycoord;
    private Orientation orientation;

    private final List<CommandType> commands;
    private final int lawnLength;
    private final int lawnWidth;


    public static Mower inputCommandParser(String position, String charSequence, int lawnLength, int lawnWidth) {
        String[] config = validateMowerPosition(position);
        List<CommandType> commands = validateCommandSequence(charSequence);

        return (new Mower(parseInt(config[0]), parseInt(config[1]), ORIENTATION_NAME.get(config[2]),
                commands, lawnLength, lawnWidth));
    }


    public void pickCommand() {
        if (commands.isEmpty()) {
            log.info("Command list is empty");
            return;
        }
        CommandType command = commands.get(0);
        switch (command) {
            case FORWARD:
                moveForward();
                break;
            case RIGHT:
            case LEFT:
                orientation = getNewOrientation(orientation, command);
                break;
        }
        commands.remove(0);
    }

    public static Orientation getNewOrientation(Orientation orientation, CommandType rotation) {
        switch (rotation) {
            case LEFT:
                return MOVE_LEFT.get(orientation);
            case RIGHT:
                return MOVE_RIGHT.get(orientation);
            default:
                throw new RuntimeException();
        }
    }

    private void moveForward() {
        switch (orientation) {
            case NORTH:
                if (ycoord < lawnWidth) {
                    ycoord += 1;
                }
                break;
            case SOUTH:
                if (ycoord > 0) {
                    ycoord -= 1;
                }
                break;
            case EAST:
                if (xcoord < lawnLength) {
                    xcoord += 1;
                }
                break;
            case WEST:
                if (xcoord > 0) {
                    xcoord -= 1;
                }
                break;
        }
    }

    public void executeCommand() {
        while (!commands.isEmpty()) {
            log.info("Executing commands");
            pickCommand();
        }
    }
}