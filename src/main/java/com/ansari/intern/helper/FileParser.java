package com.ansari.intern.helper;

import com.ansari.intern.domain.CommandType;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.ansari.intern.helper.MapBuilder.COMMAND_NAME;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static org.thymeleaf.util.StringUtils.split;

@Slf4j
public class FileParser {

    public static String[] validateInputFile(String content) {
        log.info(content);
        String[] lines = split(content, "\r\n");
        checkArgument((lines.length % 2) == 1, "lawn command file should have one line for lawn dimensions and 2 lines for each mower");
        checkArgument(lines[0].matches("[0-9]* [0-9]*"), "Lawn dimension should have two values, length and width");
        checkArgument(lines.length - 1 != 0,"Missing Mower details, Please provide mower information");

        return lines;
    }

    public static String[] validateMowerPosition(String position) {
        checkArgument(position.matches("[0-9]* [0-9]* [NEWS]"), "Mower initial position should have X Y Orientaion");
        log.info(position);
        return split(position, " ");
    }

    public static List<CommandType> validateCommandSequence(String charSequence) {
        checkArgument(charSequence.matches("[FLR]*"), "Valid commands are F, L, R");
        List<CommandType> commands = newArrayList();
        for (char c : charSequence.toCharArray()) {
            commands.add(COMMAND_NAME.get(c));
        }
        log.info("Command List:" + commands);
        return commands;
    }
}
