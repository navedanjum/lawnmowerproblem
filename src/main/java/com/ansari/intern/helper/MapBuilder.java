package com.ansari.intern.helper;

import com.ansari.intern.domain.CommandType;
import com.ansari.intern.domain.Orientation;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.ansari.intern.domain.CommandType.*;
import static com.ansari.intern.domain.Orientation.*;

public class MapBuilder {

    public static final Map<String, Orientation> ORIENTATION_NAME = ImmutableMap.of("N", NORTH, "S", SOUTH, "E", EAST, "W", WEST);
    public static final Map<Character, CommandType> COMMAND_NAME = ImmutableMap.of('F', FORWARD, 'L', LEFT, 'R', RIGHT);
    public static final Map<Orientation, Orientation> MOVE_RIGHT = ImmutableMap.of(NORTH, EAST, EAST, SOUTH, SOUTH, WEST, WEST, NORTH);
    public static final Map<Orientation, Orientation> MOVE_LEFT = ImmutableMap.of(NORTH, WEST, WEST, SOUTH, SOUTH, EAST, EAST, NORTH);

    public static Object getKeyFromValue(Map map, Object value) {
        for (Object o : map.keySet()) {
            if (map.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }

}
