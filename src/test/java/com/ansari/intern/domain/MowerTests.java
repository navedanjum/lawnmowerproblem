package com.ansari.intern.domain;

import static com.google.common.collect.Lists.*;
import static com.ansari.intern.domain.CommandType.*;
import static com.ansari.intern.domain.Orientation.*;
import static org.fest.assertions.Assertions.*;
import static org.unitils.reflectionassert.ReflectionAssert.*;
import java.util.ArrayList;

import org.junit.Test;

public class MowerTests {

    @Test
    public void initializeMower() {
        Mower mower = new Mower(3, 4, SOUTH, newArrayList(RIGHT, FORWARD, FORWARD, FORWARD, FORWARD), 10, 15);

        assertThat(mower.getXcoord()).isEqualTo(3);
        assertThat(mower.getYcoord()).isEqualTo(4);
        assertThat(mower.getOrientation()).isEqualTo(SOUTH);
        assertThat(mower.getCommands()).containsExactly(RIGHT, FORWARD,FORWARD, FORWARD, FORWARD);
    }

    @Test
    public void readMowerInitialPositionAndCommands() {
        Mower mower = Mower.inputCommandParser("0 1 N", "RFRFFLF", 10, 15);

        assertReflectionEquals(mower, new Mower(0, 1, NORTH, newArrayList(RIGHT, FORWARD, RIGHT, FORWARD, FORWARD, LEFT, FORWARD), 10, 15));
    }


    @Test
    public void testCorrectOrientationChange() {
        assertThat(Mower.getNewOrientation(NORTH, RIGHT)).isEqualTo(EAST);
        assertThat(Mower.getNewOrientation(NORTH, LEFT)).isEqualTo(WEST);

        assertThat(Mower.getNewOrientation(EAST, RIGHT)).isEqualTo(SOUTH);
        assertThat(Mower.getNewOrientation(EAST, LEFT)).isEqualTo(NORTH);

        assertThat(Mower.getNewOrientation(SOUTH, RIGHT)).isEqualTo(WEST);
        assertThat(Mower.getNewOrientation(SOUTH, LEFT)).isEqualTo(EAST);

        assertThat(Mower.getNewOrientation(WEST, RIGHT)).isEqualTo(NORTH);
        assertThat(Mower.getNewOrientation(WEST, LEFT)).isEqualTo(SOUTH);

    }

    @Test
    public void changeOrientation() {
        Mower mower = new Mower(1, 1, SOUTH, newArrayList(RIGHT, FORWARD, LEFT), 10, 15);
        mower.pickCommand();

        assertReflectionEquals(mower, new Mower(1, 1, WEST, newArrayList(FORWARD, LEFT), 10, 15));
    }

    @Test
    public void moveOneStep() {
        Mower mower = new Mower(4, 4, NORTH, newArrayList(FORWARD, LEFT), 10, 15);
        mower.pickCommand();

        assertReflectionEquals(mower, new Mower(4, 5, NORTH, newArrayList(LEFT), 10, 15));
    }

    @Test
    public void runAllCommands() {
        Mower mower = new Mower(4, 4, NORTH, newArrayList(FORWARD, LEFT, FORWARD), 10, 15);
        mower.executeCommand();

        assertReflectionEquals(mower, new Mower(3, 5, WEST, new ArrayList<>(), 10, 15));
    }

    @Test
    public void IgnoreForwardAtNorthBoundary() {
        Mower mower = new Mower(3, 5, NORTH, newArrayList(FORWARD, LEFT, FORWARD), 5, 5);
        mower.pickCommand();

        assertReflectionEquals(mower, new Mower(3, 5, NORTH, newArrayList(LEFT, FORWARD), 5, 5));
    }

    @Test
    public void IgnoreForwardAtSouthBoundary() {
        Mower mower = new Mower(3, 0, SOUTH, newArrayList(FORWARD, LEFT), 5, 5);
        mower.pickCommand();

        assertReflectionEquals(mower, new Mower(3, 0, SOUTH, newArrayList(LEFT), 5, 5));
    }

    @Test
    public void IgnoreForwardAtEastBoundary() {
        Mower mower = new Mower(4, 0, EAST, newArrayList(FORWARD, FORWARD), 5, 5);
        mower.pickCommand();
        mower.pickCommand();

        assertReflectionEquals(mower, new Mower(5, 0, EAST, new ArrayList<>(), 5, 5));
    }

    @Test
    public void IgnoreForwardAtWestBoundary() {
        Mower mower = new Mower(1, 0, WEST, newArrayList(FORWARD, FORWARD, FORWARD), 5, 5);
        mower.pickCommand();
        mower.pickCommand();
        mower.pickCommand();

        assertReflectionEquals(mower, new Mower(0, 0, WEST, new ArrayList<>(), 5, 5));
    }

    /*Negative Tests */
    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForInvalidCommands() {
        Mower.inputCommandParser("1 2 N", "FFRXYLL", 5, 6);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsExceptionForInvalidInitialPosition() {
        Mower.inputCommandParser("1 2 3 N", "", 10, 11);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsExceptionForInitialPositionOutsideLawn() {
        Mower.inputCommandParser("10 20  N", "", 10, 11);
    }

}
