package com.ansari.intern.domain;

import org.junit.Test;
import static com.ansari.intern.domain.Orientation.*;
import static com.ansari.intern.helper.MapBuilder.ORIENTATION_NAME;
import static com.ansari.intern.helper.MapBuilder.getKeyFromValue;
import static org.fest.assertions.Assertions.assertThat;

public class LawnTests {
/*Positive Tests */
    @Test
    public void initializeLawn() {
        Lawn lawn = new Lawn(30, 25, null);
        assertThat(lawn.getLength()).isEqualTo(30);
        assertThat(lawn.getWidth()).isEqualTo(25);

    }

    @Test
    public void addOneLawnMower() {
        Lawn lawn = Lawn.inputFileParser("10 10\n" + "1 2 E\n" + "RFRFRFRFRFF");
        assertThat(lawn.getMowers()).hasSize(1);
    }

    @Test
    public void addMultipleLawnMower() {
        Lawn lawn = Lawn.inputFileParser("100 50\n" + "17 2 W\n" + "RFRFRFFRFRFF\n" +
                "3 3 E\n" + "FRFFFLFFLFLFF\n" + "27 2 W\n" + "RFRFRFFRFRFF");

        assertThat(lawn.getMowers()).hasSize(3);
    }


    @Test
    public void handleBothWindowsAndUnixNewLineCharacter() {
        Lawn lawn = Lawn.inputFileParser("100 50\r\n" + "17 2 W\r\n" + "RFRFRFFRFRFF\r\n" +
                "3 3 E\r\n" + "FRFFFLFFLFLFF\n" + "27 2 W\r\n" + "RFRFRFFRFRFF");

        assertThat(lawn.getMowers()).hasSize(3);
    }

    @Test
    public void runLawnMowers() {
        Lawn lawn = Lawn.inputFileParser("5 5\n" + "1 2 N\n" + "LFLFLFLFF\n" + "3 3 E\n" + "FFRFFRFRRF");
        lawn.startMower();

        assertThat(lawn.getMowers()).hasSize(2);

        assertThat(lawn.getMowers().get(0).getXcoord()).isEqualTo(1);
        assertThat(lawn.getMowers().get(0).getYcoord()).isEqualTo(3);
        assertThat(lawn.getMowers().get(0).getOrientation()).isEqualTo(NORTH);

        assertThat(lawn.getMowers().get(1).getXcoord()).isEqualTo(5);
        assertThat(lawn.getMowers().get(1).getYcoord()).isEqualTo(1);
        assertThat(lawn.getMowers().get(1).getOrientation()).isEqualTo(EAST);
    }

    @Test
    public void findKeyFromValue(){
        assertThat(getKeyFromValue(ORIENTATION_NAME, NORTH)).isEqualTo("N");
        assertThat(getKeyFromValue(ORIENTATION_NAME, SOUTH)).isEqualTo("S");
        assertThat(getKeyFromValue(ORIENTATION_NAME, EAST)).isEqualTo("E");
        assertThat(getKeyFromValue(ORIENTATION_NAME, WEST)).isEqualTo("W");
    }

/* Negative Tests */
    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForEmptyInputFile() {
        Lawn.inputFileParser("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForNotTwoLinesPerMower() {
        Lawn.inputFileParser("5 5\n3 2 N\nRFFR\n4 3 W");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForDirtyLawnDimension() {
        Lawn.inputFileParser("A 5");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForMoreThanTwoValuesInLawnDimension() {
        Lawn.inputFileParser("10 12 16 18");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionForOnlyOneValueInLawnDimension() {
        Lawn.inputFileParser("10");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionNegativeLawnDimension() {
        Lawn.inputFileParser("-10 -10");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionZeroMowerDetails() {
        Lawn.inputFileParser("10 10");
    }
}